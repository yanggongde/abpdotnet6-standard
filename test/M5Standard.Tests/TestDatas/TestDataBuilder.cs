using M5Standard.EntityFrameworkCore;

namespace M5Standard.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly M5StandardDbContext _context;

        public TestDataBuilder(M5StandardDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            //create test data here...
        }
    }
}