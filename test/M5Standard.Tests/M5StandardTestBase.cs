﻿using System;
using System.Threading.Tasks;
using Abp.TestBase;
using M5Standard.EntityFrameworkCore;
using M5Standard.Tests.TestDatas;

namespace M5Standard.Tests
{
    public class M5StandardTestBase : AbpIntegratedTestBase<M5StandardTestModule>
    {
        public M5StandardTestBase()
        {
            UsingDbContext(context => new TestDataBuilder(context).Build());
        }

        protected virtual void UsingDbContext(Action<M5StandardDbContext> action)
        {
            using (var context = LocalIocManager.Resolve<M5StandardDbContext>())
            {
                action(context);
                context.SaveChanges();
            }
        }

        protected virtual T UsingDbContext<T>(Func<M5StandardDbContext, T> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<M5StandardDbContext>())
            {
                result = func(context);
                context.SaveChanges();
            }

            return result;
        }

        protected virtual async Task UsingDbContextAsync(Func<M5StandardDbContext, Task> action)
        {
            using (var context = LocalIocManager.Resolve<M5StandardDbContext>())
            {
                await action(context);
                await context.SaveChangesAsync(true);
            }
        }

        protected virtual async Task<T> UsingDbContextAsync<T>(Func<M5StandardDbContext, Task<T>> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<M5StandardDbContext>())
            {
                result = await func(context);
                context.SaveChanges();
            }

            return result;
        }
    }
}
