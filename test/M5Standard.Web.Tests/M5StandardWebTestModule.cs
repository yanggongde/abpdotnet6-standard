using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using M5Standard.Web;
namespace M5Standard.Web.Tests
{
    [DependsOn(
        typeof(M5StandardWebModule),
        typeof(AbpAspNetCoreTestBaseModule)
        )]
    public class M5StandardWebTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(M5StandardWebTestModule).GetAssembly());
        }
    }
}