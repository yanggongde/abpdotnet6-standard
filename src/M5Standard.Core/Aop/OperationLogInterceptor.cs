﻿using Abp.Auditing;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Castle.DynamicProxy;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using System.Diagnostics;
using M5Standard.Entity;
using M5Standard.Exceptions;
using M5Extend.Tool;
using M5Extend.Extensions;
using M5Extend.IP2Region;
using M5Standard.Core.Jwt;

namespace M5Standard.Core.Aop
{
    /// <summary>
    /// 结果缓存组件实现
    /// 注意标注方法必须有返回值
    /// </summary>
    [IgnoreScan]
    public class OperationLogInterceptor : AbpInterceptorBase, ITransientDependency
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRepository<OperationLog, long> _auditLog;
        private readonly IpSearcher _ipSearcher;

        public OperationLogInterceptor(IRepository<OperationLog, long> auditLog,
            IHttpContextAccessor httpContextAccessor,
            IpSearcher ipSearcher)
        {
            _httpContextAccessor = httpContextAccessor;
            _auditLog = auditLog;
            _ipSearcher = ipSearcher;
        }



        public override void InterceptSynchronous(IInvocation invocation)
        {
            var (isDefind, definds) = invocation.IsDefindOperationLog();
            if (!isDefind)
            {
                invocation.Proceed();
                return;
            }

            Stopwatch stopwatch = new Stopwatch();

            try
            {
                stopwatch.Start();
                invocation.Proceed();
                stopwatch.Stop();
                SaveLog(invocation, stopwatch, definds);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.InnerException == null ?
                                       ex.Message :
                                       ex.InnerException.Message;
                stopwatch.Stop();
                SaveExceptionLog(invocation, stopwatch, exceptionMessage, definds);

                throw new OperationLogException($"OperationLog捕获到异常：{exceptionMessage}", ex);
            }
            finally
            {
                stopwatch = null;
            }
        }

        protected override async Task InternalInterceptAsynchronous(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();

            var (isDefind, definds) = invocation.IsDefindOperationLog();
            if (!isDefind)
            {
                proceedInfo.Invoke();
                var task = (Task)invocation.ReturnValue;
                await task;
            }

            Stopwatch stopwatch = new Stopwatch();
            try
            {
                stopwatch.Start();
                proceedInfo.Invoke();
                var task = (Task)invocation.ReturnValue;
                await task;

                stopwatch.Stop();
                await SaveLogAsync(invocation, stopwatch, definds);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.InnerException == null ?
                                       ex.Message :
                                       ex.InnerException.Message;
                stopwatch.Stop();
                await SaveExceptionLogAsync(invocation, stopwatch, exceptionMessage, definds);

                throw new OperationLogException($"OperationLog捕获到异常：{exceptionMessage}", ex);
            }
            finally
            {
                stopwatch = null;
            }

            return;
        }

        protected override async Task<TResult> InternalInterceptAsynchronous<TResult>(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();

            var (isDefind, definds) = invocation.IsDefindOperationLog();
            if (!isDefind)
            {
                proceedInfo.Invoke();
                var taskResult = (Task<TResult>)invocation.ReturnValue;
                return await taskResult.ConfigureAwait(false);
            }

            TResult result;
            Stopwatch stopwatch = new Stopwatch();

            try
            {

                stopwatch.Start();
                proceedInfo.Invoke();
                var taskResult = (Task<TResult>)invocation.ReturnValue;
                result = await taskResult.ConfigureAwait(false);
                stopwatch.Stop();
                await SaveLogAsync(invocation, stopwatch, definds);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.InnerException == null ?
                                       ex.Message :
                                       ex.InnerException.Message;
                stopwatch.Stop();
                await SaveExceptionLogAsync(invocation, stopwatch, exceptionMessage, definds);

                throw new OperationLogException($"OperationLog捕获到异常：{exceptionMessage}", ex);
            }
            finally
            {
                stopwatch = null;
            }


            return result;
        }


        private OperationLog BuildOperationLog(IInvocation invocation, Stopwatch stopwatch, OperationLogAttribute defind)
        {
            var user = _httpContextAccessor.HttpContext.GetLoginInfo();
            var ip = _httpContextAccessor.HttpContext.M5_GetClientIP();
            return new OperationLog
            {
                Id = M5_Snowflake.CreateId(),
                BrowserInfo = _httpContextAccessor.HttpContext.M5_GetClientBrowser(),
                ClientIp = ip,
                ClientIpAddress = _ipSearcher.BinarySearch(ip)?.Region?.M5_ResolvIpAddress(),
                ClientUrl = FilterLongStr(_httpContextAccessor.HttpContext.Request.Path, 2000),
                ClientName = FilterLongStr(user?.UserName, 250),
                Exception = "",
                ExecutionDuration = stopwatch.Elapsed.TotalMilliseconds.M5_ConvertInt(),
                ExecutionTime = DateTime.Now,
                MethodName = FilterLongStr(invocation.Method.Name, 250),
                ServiceName = FilterLongStr(invocation.MethodInvocationTarget.DeclaringType.FullName, 500),
                MethodSummary = FilterLongStr(defind.ActionSummary, 500),
                Parameters = FilterParameters(invocation, defind),
                Token = user?.Token,
                UserId = user?.UserId
            };
        }

        private OperationLog BuildOperationExceptionLog(IInvocation invocation, Stopwatch stopwatch, string exmessage, OperationLogAttribute defind)
        {
            var user = _httpContextAccessor.HttpContext.GetLoginInfo();
            var ip = _httpContextAccessor.HttpContext.M5_GetClientIP();
            return new OperationLog
            {
                Id = M5_Snowflake.CreateId(),
                BrowserInfo = _httpContextAccessor.HttpContext.M5_GetClientBrowser(),
                ClientIp = ip,
                ClientIpAddress = _ipSearcher.BinarySearch(ip)?.Region?.M5_ResolvIpAddress(),
                ClientUrl = FilterLongStr(_httpContextAccessor.HttpContext.Request.Path, 2000),
                ClientName = FilterLongStr(user?.UserName, 250),
                Exception = FilterLongStr(exmessage, 4000),
                ExecutionDuration = stopwatch.Elapsed.TotalMilliseconds.M5_ConvertInt(),
                ExecutionTime = DateTime.Now,
                MethodName = FilterLongStr(invocation.Method.Name, 250),
                ServiceName = FilterLongStr(invocation.MethodInvocationTarget.DeclaringType.FullName, 250),
                MethodSummary = FilterLongStr(defind.ActionSummary, 500),
                Parameters = FilterParameters(invocation, defind),
                Token = user?.Token,
                UserId = user?.UserId
            };
        }

        /// <summary>
        /// 保存日志
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="summary"></param>
        private void SaveLog(IInvocation invocation, Stopwatch stopwatch, OperationLogAttribute defind)
        {
            var log = BuildOperationLog(invocation, stopwatch, defind);
            _auditLog.Insert(log);
        }

        /// <summary>
        /// 保存日志
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="summary"></param>
        private async Task SaveLogAsync(IInvocation invocation, Stopwatch stopwatch, OperationLogAttribute defind)
        {
            var log = BuildOperationLog(invocation, stopwatch, defind);
            await _auditLog.InsertAsync(log);
        }

        /// <summary>
        /// 保存异常日志
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="summary"></param>
        private void SaveExceptionLog(IInvocation invocation, Stopwatch stopwatch, string exmessage, OperationLogAttribute defind)
        {
            var log = BuildOperationExceptionLog(invocation, stopwatch, exmessage, defind);
            _auditLog.Insert(log);
        }

        /// <summary>
        /// 保存异常日志
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="summary"></param>
        private async Task SaveExceptionLogAsync(IInvocation invocation, Stopwatch stopwatch, string exmessage, OperationLogAttribute defind)
        {
            var log = BuildOperationExceptionLog(invocation, stopwatch, exmessage, defind);
            await _auditLog.InsertAsync(log);
        }


        /// <summary>
        /// 过滤日志中的密码明文
        /// </summary>
        /// <param name="auditInfo"></param>
        /// <returns></returns>
        private string FilterParameters(IInvocation invocation, OperationLogAttribute defind)
        {
            if (defind.IgnoreParameter)
            {
                return string.Empty;
            }

            var argsStr = invocation.Arguments.M5_ObjectToJson();
            if (argsStr.Length > 5000)
            {
                argsStr = argsStr.Substring(0, 5000);
            }
            return argsStr;
        }

        /// <summary>
        /// 过滤字符串长度
        /// </summary>
        /// <param name="str"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private string FilterLongStr(string str, int size)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            if (str.Length > size)
            {
                str = str.Substring(0, size);
            }

            return str;
        }
    }



    /// <summary>
    /// 登录用户存储信息
    /// </summary>
    [IgnoreScan]
    public class OperationLogContext
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long AccountId { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 当前登录token
        /// </summary>        
        public string Token { get; set; }
    }
}
