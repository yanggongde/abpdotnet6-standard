﻿using Abp.Dependency;
using Castle.Core;
using Castle.DynamicProxy;
using Castle.MicroKernel;
using CSRedis;
using M5Extend.Extensions;
using M5Standard.Exceptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace M5Standard.Core.Aop
{
    [IgnoreScan]
    public class DuplicateInterceptor : AbpInterceptorBase, ITransientDependency
    {
        private readonly CSRedisClient _redis;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public DuplicateInterceptor(CSRedisClient redis,
                                    IHttpContextAccessor httpContextAccessor)
        {
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }

        public override void InterceptSynchronous(IInvocation invocation)
        {
            var (isDefind, definds) = invocation.IsDefindDiplicate();
            if (!isDefind)
            {
                invocation.Proceed();
                return;
            }

            var lockKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext, invocation.Arguments, definds.Key, definds.KeyFormatNames);

            try
            {
                if (!_redis.M5_SetNX(lockKey, lockKey, definds.ExpireSecond))
                {
                    throw new LogicException(string.IsNullOrEmpty(definds.ErrorMessage) ? M5StandardConsts.DefaultAuplicationMessage : definds.ErrorMessage);
                }
                else
                {
                    invocation.Proceed();
                }
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.InnerException == null ?
                                       ex.Message :
                                       ex.InnerException.Message;

                throw new LogicException(exceptionMessage, ex);
            }
            finally
            {
                //确保在执行完毕后删除锁
                _redis.Del(lockKey);
            }
        }

        protected override async Task InternalInterceptAsynchronous(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();
            var (isDefind, definds) = invocation.IsDefindDiplicate();
            if (!isDefind)
            {
                proceedInfo.Invoke();
                var task = (Task)invocation.ReturnValue;
                await task;
                return;
            }

            var lockKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext, invocation.Arguments, definds.Key, definds.KeyFormatNames);

            try
            {
                if (!await _redis.M5_SetNXAsync(lockKey, lockKey, definds.ExpireSecond))
                {
                    throw new LogicException(string.IsNullOrEmpty(definds.ErrorMessage) ? M5StandardConsts.DefaultAuplicationMessage : definds.ErrorMessage);
                }
                else
                {
                    proceedInfo.Invoke();
                    var task = (Task)invocation.ReturnValue;
                    await task;
                }
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.InnerException == null ?
                                       ex.Message :
                                       ex.InnerException.Message;

                throw new LogicException(exceptionMessage, ex);
            }
            finally
            {
                //确保在执行完毕后删除锁
                await _redis.DelAsync(lockKey);
            }
        }

        protected override async Task<TResult> InternalInterceptAsynchronous<TResult>(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();
            var (isDefind, definds) = invocation.IsDefindDiplicate();
            if (!isDefind)
            {
                proceedInfo.Invoke();
                var taskResult = (Task<TResult>)invocation.ReturnValue;
                return await taskResult.ConfigureAwait(false);
            }


            var lockKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext, invocation.Arguments, definds.Key, definds.KeyFormatNames);
            TResult result = default;
            try
            {
                if (!await _redis.M5_SetNXAsync(lockKey, lockKey, definds.ExpireSecond))
                {
                    throw new LogicException(string.IsNullOrEmpty(definds.ErrorMessage) ? M5StandardConsts.DefaultAuplicationMessage : definds.ErrorMessage);
                }
                else
                {
                    proceedInfo.Invoke();
                    var taskResult = (Task<TResult>)invocation.ReturnValue;
                    result = await taskResult.ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.InnerException == null ?
                                       ex.Message :
                                       ex.InnerException.Message;

                throw new LogicException(exceptionMessage, ex);
            }
            finally
            {
                //确保在执行完毕后删除锁
                await _redis.DelAsync(lockKey);
            }

            return result;
        }
    }
}
