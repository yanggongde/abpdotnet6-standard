﻿using Abp.Dependency;
using Castle.Core;
using Castle.DynamicProxy;
using Castle.MicroKernel;
using CSRedis;
using M5Extend.Extensions;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace M5Standard.Core.Aop
{
    [IgnoreScan]
    public class CachedInterceptor : AbpInterceptorBase, ITransientDependency
    {
        private readonly CSRedisClient _redis;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CachedInterceptor(CSRedisClient redis,
                                    IHttpContextAccessor httpContextAccessor)
        {
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }

        public override void InterceptSynchronous(IInvocation invocation)
        {
            var (isDefind, definds) = invocation.IsDefindCached();
            if (!isDefind)
            {
                invocation.Proceed();
                return;
            }
            var cacheKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext, invocation.Arguments, definds.Key, definds.KeyFormatNames);

            if (_redis.Exists(cacheKey))
            {
                var cacheData = _redis.Get(cacheKey);
                if (!string.IsNullOrEmpty(cacheData) && invocation.Method.ReturnType != typeof(void))
                {
                    try
                    {
                        invocation.ReturnValue = cacheData.M5_JsonToObject(invocation.Method.ReturnType);

                    }
                    catch (Exception)
                    {
                        invocation.Proceed();
                        _redis.Set(cacheKey, invocation.ReturnValue, definds.ExpireSecond);
                    }

                }
            }
            else
            {
                invocation.Proceed();
                _redis.Set(cacheKey, invocation.ReturnValue, definds.ExpireSecond);
            }

        }

        protected override async Task InternalInterceptAsynchronous(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();

            proceedInfo.Invoke();
            var task = (Task)invocation.ReturnValue;
            await task;
            return;
        }

        protected override async Task<TResult> InternalInterceptAsynchronous<TResult>(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();
            var (isDefind, definds) = invocation.IsDefindCached();

            if (!isDefind)
            {
                proceedInfo.Invoke();
                var taskResult = (Task<TResult>)invocation.ReturnValue;
                return await taskResult.ConfigureAwait(false);
            }

            var cacheKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext, invocation.Arguments, definds.Key, definds.KeyFormatNames);
            TResult result;

            if (await _redis.ExistsAsync(cacheKey))
            {
                try
                {
                    result = await _redis.GetAsync<TResult>(cacheKey);
                }
                catch (Exception)
                {
                    proceedInfo.Invoke();
                    var taskResult = (Task<TResult>)invocation.ReturnValue;
                    result = await taskResult.ConfigureAwait(false);
                    await _redis.SetAsync(cacheKey, result, definds.ExpireSecond);
                }
            }
            else
            {
                proceedInfo.Invoke();
                var taskResult = (Task<TResult>)invocation.ReturnValue;
                result = await taskResult.ConfigureAwait(false);
                await _redis.SetAsync(cacheKey, result, definds.ExpireSecond);
            }
            return result;
        }
    }
}
