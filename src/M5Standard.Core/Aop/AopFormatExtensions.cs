﻿using M5Extend.Extensions;
using M5Standard.Core.Jwt;
using M5Standard.Exceptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace M5Standard.Core.Aop
{
    [IgnoreScan]
    public static class AopFormatExtensions
    {
        /// <summary>
        /// Aop方法拦截构建格式化字符串key
        /// </summary>
        /// <param name="method">拦截的方法</param>
        /// /// <param name="context">请求上下文</param>
        /// <param name="args">方法的参数</param>
        /// <param name="key">构建格式化字符串</param>
        /// <param name="keyFormatNames">格式化字符替换参数</param>
        /// <returns></returns>
        public static string BuildAopFormatKey(this MethodInfo method, HttpContext context, object[] args, string key, string[] keyFormatNames = null)
        {
            return string.IsNullOrEmpty(key) ? BuildAutoKey(context, method, args) :
                                               BuildFiexdKey(method, args, key, keyFormatNames);

        }

        /// <summary>
        /// 构建预定义key
        /// </summary>
        /// <param name="method"></param>
        /// <param name="args"></param>
        /// <param name="key"></param>
        /// <param name="keyFormatNames"></param>
        /// <returns></returns>
        private static string BuildFiexdKey(this MethodInfo method, object[] args, string key, string[] keyFormatNames = null)
        {
            if (keyFormatNames == null || !keyFormatNames.Any())
            {
                return key;
            }

            var parameters = method.GetParameters().Select((u, i) => new MethodParameterInfo
            {
                Parameter = u,
                Name = u.Name,
                Value = args[i]
            }).ToList();

            var formatValues = new List<object>();

            foreach (var item in keyFormatNames)
            {
                formatValues.Add(GetInnerValue(item, parameters));
            }

            return string.Format(key, formatValues.ToArray());
        }

        /// <summary>
        /// 根据业务方法参数自动构建key
        /// </summary>
        /// <param name="method"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string BuildAutoKey(this HttpContext context, MethodInfo method, object[] args)
        {
            var token = context.GetJwtToken();
            var args_fullName = $"{method.DeclaringType.FullName}.{method.Name}";
            var args_token = string.IsNullOrEmpty(token) ? "anonymous" : token;
            var args_JsonMd5 = ((args == null ? "[]" : args.M5_ObjectToJson()) + args_token).M5_StringToMd5();
            return string.Format(M5CacheKey.Aop, (args_fullName + ":" + args_JsonMd5));
        }


        /// <summary>
        /// 获取参数内部实体对应的格式化值
        /// </summary>
        /// <returns></returns>
        private static object GetInnerValue(this string itemName, List<MethodParameterInfo> parameters)
        {

            if (itemName.IsInner())
            {
                var itemNameArr = itemName.Split('.');
                var itemNameArrCount = itemNameArr.Count();

                object result = null;
                Type itemType = null;
                object itemDatadata = null;
                int index = 1;

                foreach (var sp in itemNameArr)
                {
                    if (itemType == null)
                    {
                        var defaultItem = parameters.FirstOrDefault(s => s.Name == sp);
                        if (defaultItem == null)
                        {
                            throw new LogicException($"Aop Key格式化器找不到对应:{itemName}-{sp}的值");
                        }

                        itemType = defaultItem.Value.GetType();
                        itemDatadata = defaultItem.Value;

                    }
                    else
                    {
                        var cloumProperty = itemType.GetProperty(sp);
                        if (cloumProperty == null)
                        {
                            throw new LogicException($"Aop Key格式化器找不到对应:{itemName}-{sp}的值");
                        }
                        itemDatadata = cloumProperty.GetValue(itemDatadata, null);
                        itemType = itemDatadata.GetType();
                    }

                    if (index == itemNameArrCount)
                    {
                        result = itemDatadata;
                        break;
                    }

                    index++;
                }

                return result;
            }
            else
            {
                var innerValue = parameters.FirstOrDefault(k => k.Name == itemName);
                if (innerValue == null)
                {
                    throw new LogicException($"Aop Key格式化器找不到对应:{itemName}的值");
                }
                return innerValue.Value;
            }
        }

        /// <summary>
        /// 判断是否需要从参数内部实体查找
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        private static bool IsInner(this string itemName)
        {
            return itemName.Contains(".");
        }


    }

    /// <summary>
    /// 方法参数信息
    /// </summary>
    [IgnoreScan]
    public class MethodParameterInfo
    {
        /// <summary>
        /// 参数
        /// </summary>
        internal ParameterInfo Parameter { get; set; }

        /// <summary>
        /// 参数名
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// 参数值
        /// </summary>
        internal object Value { get; set; }
    }
}
