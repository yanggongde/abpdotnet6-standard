﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core.Aop
{
    [IgnoreScan]
    public static class AttributeExtensions
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invocation"></param>
        /// <returns></returns>
        public static (bool, OperationLogAttribute) IsDefindOperationLog(this IInvocation invocation)
        {
            return invocation.IsDefindTAttribute<OperationLogAttribute>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invocation"></param>
        /// <returns></returns>
        public static (bool, DuplicationAttribute) IsDefindDiplicate(this IInvocation invocation)
        {
            return invocation.IsDefindTAttribute<DuplicationAttribute>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invocation"></param>
        /// <returns></returns>
        public static (bool, CachedAttribute) IsDefindCached(this IInvocation invocation)
        {
            return invocation.IsDefindTAttribute<CachedAttribute>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invocation"></param>
        /// <returns></returns>
        public static (bool, IEnumerable<CacheRemoveAttribute>) IsDefindCacheRemove(this IInvocation invocation)
        {
            return invocation.IsDefindTAttributes<CacheRemoveAttribute>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="invocation"></param>
        /// <returns></returns>
        public static (bool, TAttribute) IsDefindTAttribute<TAttribute>(this IInvocation invocation) where TAttribute : Attribute
        {
            TAttribute defind = default;
            var isDefind = invocation.MethodInvocationTarget.IsDefined(typeof(TAttribute));
            if (isDefind)
            {
                defind = invocation.MethodInvocationTarget.GetCustomAttribute<TAttribute>();
            }

            return (isDefind, defind);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="invocation"></param>
        /// <returns></returns>
        public static (bool, IEnumerable<TAttribute>) IsDefindTAttributes<TAttribute>(this IInvocation invocation) where TAttribute : Attribute
        {
            IEnumerable<TAttribute> defind = default;
            var isDefind = invocation.MethodInvocationTarget.IsDefined(typeof(TAttribute));
            if (isDefind)
            {
                defind = invocation.MethodInvocationTarget.GetCustomAttributes<TAttribute>();
            }

            return (isDefind, defind);
        }
    }
}
