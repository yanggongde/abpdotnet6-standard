﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core.Aop
{
    /// <summary>
    /// 标注方法删除特定缓存（可多个标记）    
    /// </summary>

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    [IgnoreScan]
    public class CacheRemoveAttribute : Attribute
    {
        /// <summary>
        /// 是否开启批量前缀删除(默认关闭)
        /// </summary>
        public bool EnableStartWithBatchRemove { get; set; }

        /// <summary>
        /// 删除的key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 删除key的format格式化制定参数name,空则按照固定Key
        /// </summary>
        public string[] KeyFormatNames { get; set; }


        public CacheRemoveAttribute(string key)
        {
            Key = key;
            KeyFormatNames = null;
            EnableStartWithBatchRemove = false;
        }

        public CacheRemoveAttribute(string key, params string[] keyFormatNames)
        {
            Key = key;
            KeyFormatNames = keyFormatNames;
            EnableStartWithBatchRemove = false;
        }

        public CacheRemoveAttribute(string key, bool enableStartWithBatchRemove)
        {
            Key = key;
            KeyFormatNames = null;
            EnableStartWithBatchRemove = enableStartWithBatchRemove;
        }

        public CacheRemoveAttribute(string key, bool enableStartWithBatchRemove, params string[] keyFormatNames)
        {
            Key = key;
            KeyFormatNames = keyFormatNames;
            EnableStartWithBatchRemove = enableStartWithBatchRemove;
        }
    }
}
