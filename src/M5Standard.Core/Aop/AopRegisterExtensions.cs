﻿using Abp.Dependency;
using Castle.Core;
using Castle.MicroKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core.Aop
{
    [IgnoreScan]
    public static class AopRegisterExtensions
    {
        public static void InitializeAop(this IIocManager iocmanager)
        {
            iocmanager.InitializeOperationLog();
            iocmanager.InitializeDuplicate();
            iocmanager.InitializeCache();
            iocmanager.InitializeCacheRemoved();
        }

        public static void RegisterAopInterceptor(this IIocManager iocmanager)
        {
            iocmanager.Register(typeof(AbpAsyncDeterminationInterceptor<OperationLogInterceptor>), DependencyLifeStyle.Transient);
            iocmanager.Register(typeof(AbpAsyncDeterminationInterceptor<CachedInterceptor>), DependencyLifeStyle.Transient);
            iocmanager.Register(typeof(AbpAsyncDeterminationInterceptor<CacheRemoveInterceptor>), DependencyLifeStyle.Transient);
            iocmanager.Register(typeof(AbpAsyncDeterminationInterceptor<DuplicateInterceptor>), DependencyLifeStyle.Transient);
        }

        private static void InitializeOperationLog(this IIocManager iocmanager)
        {
            iocmanager.IocContainer.Kernel.ComponentRegistered += OperationLogKernel_ComponentRegistered;
        }

        private static void InitializeCache(this IIocManager iocmanager)
        {
            iocmanager.IocContainer.Kernel.ComponentRegistered += CacheKernel_ComponentRegistered;
        }

        private static void InitializeDuplicate(this IIocManager iocmanager)
        {
            iocmanager.IocContainer.Kernel.ComponentRegistered += DuplicationKernel_ComponentRegistered;
        }

        private static void InitializeCacheRemoved(this IIocManager iocmanager)
        {
            iocmanager.IocContainer.Kernel.ComponentRegistered += CacheRemoveKernel_ComponentRegistered;
        }

        private static void OperationLogKernel_ComponentRegistered(string key, IHandler handler)
        {
            var type = handler.ComponentModel.Implementation;

            if (type.GetTypeInfo().IsDefined(typeof(OperationLogAttribute), true) ||
                type.GetMethods().Any(m => m.IsDefined(typeof(OperationLogAttribute), true)))
            {
                handler.ComponentModel.Interceptors.Add(
                    new InterceptorReference(typeof(AbpAsyncDeterminationInterceptor<OperationLogInterceptor>))
                );
            }

        }

        private static void CacheKernel_ComponentRegistered(string key, IHandler handler)
        {
            var type = handler.ComponentModel.Implementation;

            if (type.GetTypeInfo().IsDefined(typeof(CachedAttribute), true) ||
                type.GetMethods().Any(m => m.IsDefined(typeof(CachedAttribute), true)))
            {
                handler.ComponentModel.Interceptors.Add(
                    new InterceptorReference(typeof(AbpAsyncDeterminationInterceptor<CachedInterceptor>))
                );
            }

        }

        private static void CacheRemoveKernel_ComponentRegistered(string key, IHandler handler)
        {
            var type = handler.ComponentModel.Implementation;

            if (type.GetTypeInfo().IsDefined(typeof(CacheRemoveAttribute), true) ||
                type.GetMethods().Any(m => m.IsDefined(typeof(CacheRemoveAttribute), true)))
            {
                handler.ComponentModel.Interceptors.Add(
                    new InterceptorReference(typeof(AbpAsyncDeterminationInterceptor<CacheRemoveInterceptor>))
                );
            }

        }

        private static void DuplicationKernel_ComponentRegistered(string key, IHandler handler)
        {
            var type = handler.ComponentModel.Implementation;

            if (type.GetTypeInfo().IsDefined(typeof(DuplicationAttribute), true) ||
                type.GetMethods().Any(m => m.IsDefined(typeof(DuplicationAttribute), true)))
            {
                handler.ComponentModel.Interceptors.Add(
                    new InterceptorReference(typeof(AbpAsyncDeterminationInterceptor<DuplicateInterceptor>))
                );
            }

        }
    }
}
