﻿using Abp.Dependency;
using Castle.DynamicProxy;
using CSRedis;
using M5Extend.Extensions;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core.Aop
{
    /// <summary>
    /// 移除缓存代理
    /// </summary>
    [IgnoreScan]
    public class CacheRemoveInterceptor : AbpInterceptorBase, ITransientDependency
    {
        private readonly CSRedisClient _redis;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CacheRemoveInterceptor(CSRedisClient redis,
                                    IHttpContextAccessor httpContextAccessor)
        {
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }

        public override void InterceptSynchronous(IInvocation invocation)
        {
            invocation.Proceed();

            var (isDefind, definds) = invocation.IsDefindCacheRemove();
            if (isDefind)
            {
                foreach (var defind in definds)
                {
                    var cacheKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext,invocation.Arguments, defind.Key, defind.KeyFormatNames);
                    if (defind.EnableStartWithBatchRemove)
                        _redis.M5_RemoveStartsWith(cacheKey);
                    else
                        _redis.Del(cacheKey);
                }
            }
        }

        protected override async Task InternalInterceptAsynchronous(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();
            proceedInfo.Invoke();
            var task = (Task)invocation.ReturnValue;
            await task;

            var (isDefind, definds) = invocation.IsDefindCacheRemove();
            if (isDefind)
            {
                foreach (var defind in definds)
                {
                    var cacheKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext, invocation.Arguments, defind.Key, defind.KeyFormatNames);
                    if (defind.EnableStartWithBatchRemove)
                        await _redis.M5_RemoveStartsWithAsync(cacheKey);
                    else
                        await _redis.DelAsync(cacheKey);
                }
            }
        }

        protected override async Task<TResult> InternalInterceptAsynchronous<TResult>(IInvocation invocation)
        {
            var proceedInfo = invocation.CaptureProceedInfo();
            proceedInfo.Invoke();
            var taskResult = (Task<TResult>)invocation.ReturnValue;
            var result = await taskResult.ConfigureAwait(false);

            var (isDefind, definds) = invocation.IsDefindCacheRemove();
            if (isDefind)
            {
                foreach (var defind in definds)
                {
                    var cacheKey = invocation.Method.BuildAopFormatKey(_httpContextAccessor.HttpContext, invocation.Arguments, defind.Key, defind.KeyFormatNames);
                    if (defind.EnableStartWithBatchRemove)
                        await _redis.M5_RemoveStartsWithAsync(cacheKey);
                    else
                        await _redis.DelAsync(cacheKey);
                }
            }

            return result;

        }
    }
}
