﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Entity
{
    [Table("User")]
    public class User: Entity<long>, ISoftDelete
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public virtual bool IsDeleted { get; set; }
    }
}
