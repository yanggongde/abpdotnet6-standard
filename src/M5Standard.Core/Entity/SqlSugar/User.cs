﻿using Abp.Domain.Entities;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Entity.SqlSugar
{
    [SugarTable("User")]
    public class User2
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = false)]
        public long Id { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }

        public bool IsDeleted { get; set; }

    }
}
