﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Enum
{
    public enum LoginClaims
    {
        UserId,
        UserName
    }
}
