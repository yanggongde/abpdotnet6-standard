﻿using M5Standard.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Exceptions
{
    [IgnoreScan]
    public class LogicException : Exception
    {
        public LogicException(string message) : base(message)
        {
        }

        public LogicException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
    [IgnoreScan]
    public class OperationLogException : LogicException
    {
        public OperationLogException(string message) : base(message)
        {
        }

        public OperationLogException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
    [IgnoreScan]
    public class UnauthorizedException : LogicException
    {
        public UnauthorizedException(string message) : base(message)
        {
        }
    }
    [IgnoreScan]
    public class SingleLoginException : LogicException
    {
        public SingleLoginException(string message) : base(message)
        {
        }
    }
    [IgnoreScan]
    public class NoPermissionException : LogicException
    {
        public NoPermissionException(string message) : base(message)
        {
        }
    }

}
