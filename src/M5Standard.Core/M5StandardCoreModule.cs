﻿using Abp.Dependency;
using Abp.Modules;
using Abp.Reflection.Extensions;
using M5Standard.Core;
using M5Standard.Localization;

namespace M5Standard
{
    [IgnoreScan]
    public class M5StandardCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;
            M5StandardLocalizationConfigurer.Configure(Configuration.Localization);
            Configuration.Settings.SettingEncryptionConfiguration.DefaultPassPhrase = M5StandardConsts.DefaultPassPhrase;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(M5StandardCoreModule).GetAssembly());
        }
    }
}