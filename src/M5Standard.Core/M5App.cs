﻿using M5Extend.Extensions;
using M5Extend.Tool.Jwt;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core
{
    [IgnoreScan]
    public static class M5App
    {
        static M5App()
        {
            var dependencyContext = DependencyContext.Default;

            // 需排除的程序集后缀
            var excludeAssemblyNames = new string[] {
                "M5Extend",
                ".Migrations",                
                ".Web",
                ".EntityFrameworkCore"
            };

            // 读取项目程序集或 Furion 官方发布的包，或手动添加引用的dll，或配置特定的包前缀
            Assemblies = dependencyContext.RuntimeLibraries
                .Where(u =>
                       (u.Type == "project" && !excludeAssemblyNames.Any(j => u.Name.EndsWith(j))) ||
                       (u.Type == "reference"))    // 判断是否启用引用程序集扫描
                .Select(u => u.Name.M5_GetAssembly());

            // 获取有效的类型集合
            EffectiveTypes = Assemblies.SelectMany(u => u.GetTypes()
                .Where(u => u.IsPublic && !u.IsDefined(typeof(IgnoreScanAttribute), false)));
        }

        /// <summary>
        /// 有效程序集
        /// </summary>
        public static readonly IEnumerable<Assembly> Assemblies;

        /// <summary>
        /// 有效类型
        /// </summary>
        public static readonly IEnumerable<Type> EffectiveTypes;

        /// <summary>
        /// 配置文件
        /// </summary>
        public static IConfiguration Configuration { get; private set; }

        /// <summary>
        /// 环境
        /// </summary>
        public static IWebHostEnvironment HostEnvironment { get; private set; }

        /// <summary>
        /// JWT配置
        /// </summary>
        public static JWTSettings JwtSettings { get; private set; }

        public static void Register(this IConfiguration configuration, IWebHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            HostEnvironment = hostEnvironment;
        }



        public static void RegisterJwtSettings(this JWTSettings jwtSettings)
        {
            JwtSettings = jwtSettings;
        }
    }


    /// <summary>
    /// 不被扫描和发现的特性
    /// </summary>
    /// <remarks>用于程序集扫描类型或方法时候</remarks>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Struct)]
    [IgnoreScan]
    public class IgnoreScanAttribute : Attribute
    {
    }
}
