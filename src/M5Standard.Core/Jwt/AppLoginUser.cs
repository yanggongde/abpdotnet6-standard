﻿using M5Extend.Tool.Jwt;
using M5Extend.Extensions;
using M5Standard.Core.Output;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using M5Standard.Enum;
using M5Standard.Core;
using M5Extend.IP2Region;
using CSRedis;

namespace M5Standard.Core.Jwt
{
    [IgnoreScan]
    public static class AppLoginUser
    {
        /// <summary>
        /// 获取登录Token
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetJwtToken(this HttpContext context)
        {
            return context.GetToken();
        }

        /// <summary>
        /// 获取登录的业务参数
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static LoginInfo GetLoginInfo(this HttpContext context)
        {
            var claims = context.GetContextClaims();
            var result = new LoginInfo
            {
                UserId = claims.M5_DicTryGetValue(LoginClaims.UserId.ToString()).M5_ParseLong(),
                UserName = claims.M5_DicTryGetValue(LoginClaims.UserName.ToString()),
                Token = context.GetToken()
            };

            return result;
        }

        /// <summary>
        /// 颁发Token
        /// </summary>
        /// <param name="logininfo"></param>
        /// <returns></returns>
        public static string BuildLoginToken(this LoginInfo logininfo)
        {
            var dic = new Dictionary<string, string>
            {
                {LoginClaims.UserId.ToString(),logininfo.UserId.ToString()},
                {LoginClaims.UserName.ToString(),logininfo.UserName}
            };

            return M5App.JwtSettings.CreateToken(dic);
        }

        /// <summary>
        /// 颁发Token，携带单点登录认证
        /// </summary>
        /// <param name="logininfo"></param>
        /// <returns></returns>
        public static async Task<string> BuildLoginToken(this LoginInfo logininfo, CSRedisClient redis, HttpContext context, IpSearcher ipSearcher)
        {
            var token = logininfo.BuildLoginToken();
            var isEnableSingleLogin = (M5App.Configuration["AppSettings:EnableSingleLoginCheck"]).M5_ParseBoolean();
            if (isEnableSingleLogin)
            {
                var ip = context.M5_GetClientIP();
                var ipaddress = ipSearcher.BtreeSearch(ip)?.Region?.M5_ResolvIpAddress();

                //写入单点登录中心
                await redis.DelAsync(M5CacheKey.SingleLoginById.M5_Format(logininfo.UserId));
                await redis.SetAsync(M5CacheKey.SingleLoginById.M5_Format(logininfo.UserId), new SingleLogin
                {
                    LoginIp = ip,
                    LoginTime = DateTime.Now.ToString("MM月dd日 HH:mm:ss"),
                    Token = $"Bearer {token}",
                    DevicesName = context.M5_GetBrowserName(),
                    IpAddress = ipaddress
                }, TimeSpan.FromMinutes(M5App.Configuration["JWTSettings:Lifetime"].M5_ParseInt() + 10));
            }            

            return token;
        }
    }
}
