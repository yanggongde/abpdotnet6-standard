﻿using CSRedis;
using M5Extend.IP2Region;
using M5Extend.Tool.Http;
using M5Standard.Exceptions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core
{
    [IgnoreScan]
    public static class M5CommonModel
    {
        /// <summary>
        /// 配置公共组件
        /// </summary>
        /// <param name="services"></param>
        /// <exception cref="RedisClientException"></exception>
        public static void AddCommonModel(this IServiceCollection services)
        {            
            //注册分布式缓存
            services.AddSingleton(factory =>
            {
                return new CSRedisClient(M5App.Configuration["ConnectionStrings:DistributedCache"] 
                    ?? throw new LogicException("找不到csredis配置节点"));
            });

            var filePath = Path.Combine(M5App.HostEnvironment.ContentRootPath, "AppData/ip2region.db");
            //注册ip离线查询驱动
            services.ConfigIp2region(filePath);

            //添加httpfactory支持
            services.AddHttpClient();
            services.AddTransient<M5HttpClient>();
        }
    }
}
