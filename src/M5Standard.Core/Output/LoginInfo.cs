﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core.Output
{

    public class LoginInfo
    {
        [Required(ErrorMessage = "请输入账户ID")]
        public long UserId { get; set; }

        [Required(ErrorMessage = "请输入账户名称")]
        public string UserName { get; set; }


        public string Token { get; set; }

    }

    [Serializable]
    public class SingleLogin
    {
        public string Token { get; set; }

        public string LoginTime { get; set; }

        public string LoginIp { get; set; }

        public string IpAddress { get; set; }

        public string DevicesName { get; set; }
    }

    [Serializable]
    public class SingleLoginOutput
    {
        public string Message { get; set; }

        public string LoginTime { get; set; }

        public string LoginIp { get; set; }

        public string IpAddress { get; set; }

        public string DevicesName { get; set; }
    }
}
