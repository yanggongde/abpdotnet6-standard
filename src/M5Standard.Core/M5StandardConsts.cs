﻿using M5Standard.Core;

namespace M5Standard
{
    [IgnoreScan]
    public class M5StandardConsts
    {
        public const string LocalizationSourceName = "M5Standard";

        public const string ConnectionStringName = "Default";

        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public const string DefaultPassPhrase = "22c16989f02c40408ccd773c6533dedb";

        public const int DefaultLockDisponseSecond = 180;

        public const string DefaultAuplicationMessage = "系统繁忙请稍后再试";
    }

    [IgnoreScan]
    public static class M5CacheKey
    {
        public static string Aop = "AopCache:{0}";

        public static string SingleLoginById = "singleLoginCache:{0}";
    }
}