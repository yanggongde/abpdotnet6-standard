﻿using Abp.EntityFrameworkCore;
using M5Standard.Entity;
using Microsoft.EntityFrameworkCore;

namespace M5Standard.EntityFrameworkCore
{
    public class M5StandardDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<OperationLog> OperationLog { get; set; }

        public M5StandardDbContext(DbContextOptions<M5StandardDbContext> options) 
            : base(options)
        {

        }
    }
}
