﻿using M5Standard.Configuration;
using M5Standard.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace M5Standard.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class M5StandardDbContextFactory : IDesignTimeDbContextFactory<M5StandardDbContext>
    {
        public M5StandardDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<M5StandardDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            DbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(M5StandardConsts.ConnectionStringName)
            );

            return new M5StandardDbContext(builder.Options);
        }
    }
}