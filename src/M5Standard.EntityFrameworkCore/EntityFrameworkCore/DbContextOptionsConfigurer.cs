﻿using Microsoft.EntityFrameworkCore;

namespace M5Standard.EntityFrameworkCore
{
    public static class DbContextOptionsConfigurer
    {
        public static void Configure(
            DbContextOptionsBuilder<M5StandardDbContext> dbContextOptions, 
            string connectionString
            )
        {
            /* This is the single point to configure DbContextOptions for M5StandardDbContext */
            dbContextOptions.UseSqlServer(connectionString);
        }
    }
}
