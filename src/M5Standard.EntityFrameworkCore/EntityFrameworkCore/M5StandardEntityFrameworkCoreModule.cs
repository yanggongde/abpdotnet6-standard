﻿using Abp.EntityFrameworkCore;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace M5Standard.EntityFrameworkCore
{
    [DependsOn(
        typeof(M5StandardCoreModule), 
        typeof(AbpEntityFrameworkCoreModule))]
    public class M5StandardEntityFrameworkCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(M5StandardEntityFrameworkCoreModule).GetAssembly());
        }
    }
}