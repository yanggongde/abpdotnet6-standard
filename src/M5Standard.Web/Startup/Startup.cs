﻿using System;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using Abp.EntityFrameworkCore;
using M5Standard.EntityFrameworkCore;
using Castle.Facilities.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using M5Standard.Web.Filters;
using M5Standard.Core;
using System.Reflection;
using M5Extend.Tool;
using M5Extend.Tool.Jwt;
using M5Extend.Output;
using M5Extend.IP2Region;
using Abp.AspNetCore.Mvc.Validation;
using System.Linq;

namespace M5Standard.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {

        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        /// <param name="configuration"></param>
        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            _hostingEnvironment = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile(@"appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            _configuration = builder.Build();
            _configuration.Register(_hostingEnvironment);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(fac => { return _configuration; });
            services.AddCors(option => option.AddPolicy("cors", policy => policy.WithOrigins(_configuration["AppSettings:CorsOrigins"].Split(',')).AllowAnyHeader().AllowAnyMethod().AllowCredentials()));
            services.AddMiniProfiler(options => { options.RouteBasePath = "/profiler"; }).AddEntityFramework();
            services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = $"请求授权，请求头Header必须附带key:Authorization 的token 格式： [Bearer Token]",
                    Name = "Authorization",//默认参数名称      
                    Scheme = "Bearer",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT"
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });
            services.AddCommonModel();
            services.AddJwt(_configuration).RegisterJwtSettings();
            services.AddAbpDbContext<M5StandardDbContext>(options =>
            {
                DbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
            });
           

            services.AddResponseCompression();
            services.AddControllers(options =>
            {
                options.Filters.Add<GlobalValidateOAuthFilter>();
                options.Filters.Add<GlobalResultFilterAttribute>();                
                options.Filters.Add<GlobalExceptionFilter>();
                
            }).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                options.SerializerSettings.Converters.Add(new M5_LongConverter());
            }).ConfigureApiBehaviorOptions(o =>
            {
                o.SuppressModelStateInvalidFilter = true;
            }); 


            return services.AddAbp<M5StandardWebModule>(options =>
            {
                options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig(_hostingEnvironment.IsDevelopment() ? "log4net.config" : "log4net.Production.config")
                );
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            M5_Snowflake.InitData(1, 1);
            app.UseCors("cors");
            app.UseAbp();

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/Error");

            app.UseMiniProfiler();
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.IndexStream = () => GetType().GetTypeInfo().Assembly.GetManifestResourceStream("M5Standard.Web.index.html");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "AbpZeroTemplate API V1");
            });

            app.UseResponseCompression();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
