﻿using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.Dependency;
using Abp.Modules;
using Abp.Reflection.Extensions;
using M5Extend.Tool.Http;
using M5Standard.Configuration;
using M5Standard.Core.Aop;
using M5Standard.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;

namespace M5Standard.Web
{
    [DependsOn(
        typeof(M5StandardApplicationModule),
        typeof(M5StandardEntityFrameworkCoreModule),
        typeof(AbpAspNetCoreModule))]
    public class M5StandardWebModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public M5StandardWebModule(IWebHostEnvironment env)
        {
            _appConfiguration = AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName);
        }

        public override void PreInitialize()
        {
            //注册Aop拦截
            IocManager.InitializeAop();
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(M5StandardConsts.ConnectionStringName);

            //启用mvc默认时间格式
            //Configuration.Modules.AbpAspNetCore().UseMvcDateTimeFormatForAppServices = true;
            //关闭abp默认模型验证拦截
            Configuration.Modules.AbpAspNetCore().IsValidationEnabledForControllers = false;
            //Configuration.Modules.AbpAspNetCore().SetNoCacheForAjaxResponses = false;
            Configuration.Auditing.IsEnabled = false;

            //动态APi
            //Configuration.Modules.AbpAspNetCore()
            //    .CreateControllersForAppServices(
            //        typeof(M5StandardApplicationModule).GetAssembly()
            //    );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(M5StandardWebModule).GetAssembly());
            IocManager.RegisterAopInterceptor();           
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(M5StandardWebModule).Assembly);
        }
    }
}
