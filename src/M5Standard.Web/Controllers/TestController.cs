using M5Extend.Tool.Http;
using M5Extend.Tool.Jwt;
using M5Standard.Entity;
using M5Standard.Services;
using M5Standard.Services.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using M5Extend.Output;
using M5Extend.Tool;
using M5Extend.IP2Region;
using M5Extend.Extensions;
using M5Standard.Core.Output;
using M5Standard.Core.Jwt;
using Microsoft.AspNetCore.Http;
using CSRedis;
using M5Standard.Core;
using M5Standard.Exceptions;

namespace M5Standard.Web.Controllers
{
    /// <summary>
    /// 测试
    /// </summary>
    public class TestController : M5StandardControllerBase
    {
        private readonly ITestService2 _testService2;
        private readonly TestService _testService;
        private readonly JWTSettings _JWTSettings;
        private readonly IConfiguration _configuration;
        private readonly M5HttpClient _httpClient;
        private readonly IpSearcher _IpSearcher;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly CSRedisClient _redis;
        public TestController(TestService testService,
            IOptions<JWTSettings> jWTSettings,
            IConfiguration configuration,
            ITestService2 testService2,
            IpSearcher ipSearcher,
            M5HttpClient httpClient, IHttpContextAccessor httpContextAccessor,
            CSRedisClient redis)
        {
            _testService = testService;
            _JWTSettings = jWTSettings.Value;
            _configuration = configuration;
            _testService2 = testService2;
            _httpClient = httpClient;
            _IpSearcher = ipSearcher;
            _httpContextAccessor = httpContextAccessor;
            _redis = redis;
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<string> SayHello()
        {
            return await _testService2.SayHello();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<DateTime> GetTime(LoginInfo info)
        {
            return DateTime.Now;
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<string> Login(LoginInfo info)
        {

            //这里手动写入日志，不然自动日志获取不到登录信息
            var token = await info.BuildLoginToken(_redis, _httpContextAccessor.HttpContext, _IpSearcher);
            return token;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<User> TestHttp1()
        {
            var loginInfo = new LoginInfo
            {
                UserId = 1,
                UserName = "gd"
            };
            var token = await loginInfo.BuildLoginToken(_redis, _httpContextAccessor.HttpContext, _IpSearcher);

            var httpInput = new M5HttpGet
            {
                Url = "http://localhost:5000/api/Test/GetById",
                Arguments = new { id = 33105480075184640 },
                Headers = new Dictionary<string, string> { { "Authorization", $"Bearer {token}" } }
            };
            var data = await _httpClient.GetAsync<M5Result<User>>(httpInput);
            return data.data;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task TestHttp2()
        {
            var loginInfo = new LoginInfo
            {
                UserId = 1,
                UserName = "gd"
            };
            var token = await loginInfo.BuildLoginToken(_redis, _httpContextAccessor.HttpContext, _IpSearcher);

            var httpInput = new M5HttpSend
            {
                Url = "http://localhost:5000/api/Test/Insert",
                Arguments = new User
                {
                    Id = M5_Snowflake.CreateId(),
                    Age = 123,
                    Name = "http添加"
                },
                HttpMethod = HttpMethod.Post,
                Headers = new Dictionary<string, string> { { "Authorization", $"Bearer {token}" } }
            };
            await _httpClient.SendAsync(httpInput);
        }


        [HttpGet]
        [AllowAnonymous]
        public string CreateToken()
        {

            return _JWTSettings.CreateToken(new Dictionary<string, string>()
            {
                {"Id","9527" }
            });
        }

        /// <summary>
        /// GetDescription
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<string> GetIPInfo(string ip)
        {
            var data = await _IpSearcher.BtreeSearchAsync(ip).ConfigureAwait(false);
            return data.Region.M5_ResolvIpAddress();
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual async Task<List<UserDto>> GetAll()
        {

            //Logger.Error("哈哈哈@@@@@@@@@@@@@@@@@@@@VVVVVVVVVVVVVVVVVVVVVVVVVVBBBBBBBBBBBBBBBBBBBBBBBBBB");
            //throw new UnauthorizedException("出现了一个错误AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            var key = "21321321321";
            return await _testService.GetAll2();
            //await _redis.SetAsync(key, data, 60);

            //var data2 = await _redis.GetAsync<List<UserDto>>(key);
            //return data2;
        }

        [HttpGet]
        public virtual async Task<User> GetById(long id)
        {
            var clims = HttpContext.GetContextClaims();
            var data = await _testService.GetById(id);
            return data;
        }

        [HttpPost]
        public virtual async Task<User> Insert(User user)
        {
            await _testService.Insert(user);
            return user;
        }

        [HttpPost]
        public virtual async Task Delete(long id)
        {
            await _testService.Delete(id);
        }

    }
}