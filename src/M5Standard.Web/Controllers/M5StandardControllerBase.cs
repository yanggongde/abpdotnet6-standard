using Abp.AspNetCore.Mvc.Controllers;
using Abp.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace M5Standard.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [DontWrapResult]
    [Authorize]
    public abstract class M5StandardControllerBase: AbpController
    {
        protected M5StandardControllerBase()
        {
            LocalizationSourceName = M5StandardConsts.LocalizationSourceName;
        }
    }
}