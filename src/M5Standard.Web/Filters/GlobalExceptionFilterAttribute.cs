using M5Standard.Exceptions;
using M5Extend.Output;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace M5Standard.Web.Filters
{
    /// <summary>
    /// ȫ���쳣����
    /// </summary>
    public class GlobalExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<GlobalExceptionFilter> _logger;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public GlobalExceptionFilter(ILogger<GlobalExceptionFilter> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            var message = string.IsNullOrEmpty(exception.Message) ?
                    exception.InnerException?.Message :
                    exception.Message;
            var dtoResult = new M5Result { code = 500, message = message };

            switch (exception)
            {
                case UnauthorizedException:
                    { dtoResult.code = 401; }
                    break;
                case SingleLoginException:
                    { dtoResult.code = 402; }
                    break;
                case NoPermissionException:
                    { dtoResult.code = 403; }
                    break;
                case LogicException:
                    { dtoResult.code = 400; }
                    break;
            }

            if (dtoResult.code == 500)
                _logger.LogError(message, exception);

            context.ExceptionHandled = true;
            context.Result = new ObjectResult(dtoResult);
        }
    }
}