﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using CSRedis;
using System.Collections.Generic;
using System;
using M5Standard.Core.Output;
using M5Standard;
using M5Extend.Extensions;
using Microsoft.AspNetCore.Http;
using M5Standard.Core.Jwt;
using System.Linq;
using M5Standard.Core;
using M5Standard.Exceptions;
using System.Threading.Tasks;

namespace M5Standard.Web.Filters
{
    /// <summary>
    /// API授权请求后续校验
    /// 校验顺序 JWT -> 后续校验【单点校验 ->权限校验】->参数模型校验
    /// </summary>
    public class GlobalValidateOAuthFilter : IAsyncActionFilter, IOrderedFilter
    {
        public int Order => 1;

        /// <summary>
        /// redis
        /// </summary>
        private readonly CSRedisClient _redis;
        /// <summary>
        /// 是否开启权限校验
        /// </summary>
        private readonly bool _isEnablePermission = (M5App.Configuration["AppSettings:EnablePermissionCheck"] ?? throw new Exception("尚未定义EnablePermissionCheck配置")).M5_ParseBoolean();
        /// <summary>
        /// 是否开启权限校验
        /// </summary>
        private readonly bool _isEnableSingleLogin = (M5App.Configuration["AppSettings:EnableSingleLoginCheck"] ?? throw new Exception("尚未定义EnableSingleLoginCheck配置")).M5_ParseBoolean();

        /// <summary>
        /// 请求上下文
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="redis"></param>
        /// <param name="httpContextAccessor"></param>
        public GlobalValidateOAuthFilter(CSRedisClient redis, IHttpContextAccessor httpContextAccessor)
        {
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            //Token已先验证

            //开启授权校验
            if (IsDefindAttribute<AuthorizeAttribute>(context))
            {
                var isAllowAnonymous = IsDefindAttribute<AllowAnonymousAttribute>(context);
                //单点校验
                CheckSingleLogin(context, isAllowAnonymous);
                //请求路由校验
                CheckPermission(context, isAllowAnonymous);
            }

            //模型验证
            ValidateModel(context);

            await next();
        }

        private void ValidateModel(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                foreach (var state in context.ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        throw new LogicException(error.ErrorMessage);
                    }
                }
            }
        }

        private void CheckSingleLogin(ActionExecutingContext context, bool isAllowAnonymous)
        {
            if (_isEnableSingleLogin && !isAllowAnonymous)
            {
                var singLogin = GetLoginSingleLogin(_httpContextAccessor.HttpContext);
                var token = AppLoginUser.GetJwtToken(_httpContextAccessor.HttpContext);
                if (singLogin == null || string.IsNullOrEmpty(token)) throw new UnauthorizedException($"Unauthorized");
                if (token != singLogin.Token)
                {
                    var output = singLogin.M5_MapTo<SingleLoginOutput>();
                    output.Message = "当前账号已在其他设备上登录";
                    throw new SingleLoginException(output.M5_ObjectToJson());
                }
            }
        }

        private void CheckPermission(ActionExecutingContext context, bool isAllowAnonymous)
        {
            if (isAllowAnonymous) return;
            if (!_isEnablePermission) return;

            var isSkipCheck = IsDefindAttribute<SkipPermissionCheckAttribute>(context);
            if (isSkipCheck) return;

            if (context.HttpContext.Request.Path.HasValue)
            {
                var route = context.HttpContext.Request.Path.Value.ToLower().Trim();
                var permissions = GetLoginUserPermissions(_httpContextAccessor.HttpContext);
                if (!permissions.Any(k => k == route)) throw new NoPermissionException($"访问失败，暂无{route}访问权限");
            }
        }

        private bool IsDefindAttribute<T>(ActionExecutingContext context) where T : Attribute
        {
            return context.ActionDescriptor.EndpointMetadata.OfType<T>().Any();
        }

        /// <summary>
        /// 获取当前登录用户的额API访问权限，建议从缓存中获取
        /// </summary>
        /// <returns></returns>
        private List<string> GetLoginUserPermissions(HttpContext context)
        {
            var user = context.GetLoginInfo();
            //这里获取用户的路由集合 待实现
            return new List<string>();
        }

        /// <summary>
        /// 获取当前登录用户的额API访问权限，建议从缓存中获取
        /// </summary>
        /// <returns></returns>
        private SingleLogin GetLoginSingleLogin(HttpContext context)
        {
            var user = context.GetLoginInfo();
            return _redis.Get<SingleLogin>(M5CacheKey.SingleLoginById.M5_Format(user.UserId));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuted(ActionExecutedContext context)
        {
            //操作执行后做的事情
        }
    }

    /// <summary>
    /// 标注跳过权限校验（注意不会跳过JWT校验，jwt跳过AllowAnonymousAttribute标注）
    /// 跳过JWT+权限校验标注 AllowAnonymousAttribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class SkipPermissionCheckAttribute : Attribute
    {

    }
}
