using M5Extend.Output;
using M5Standard.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading;
using System.Threading.Tasks;

namespace M5Standard.Web.Filters
{
    /// <summary>
    /// 全局统一输出
    /// </summary>
    public class GlobalResultFilterAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            base.OnResultExecuting(context);

            if (context.Result is ObjectResult objectResult)
            {
                if (!(objectResult.Value is M5Result))
                {
                    context.Result = new ObjectResult(new M5Result<object>(200, "操作成功", objectResult.Value));
                }
            }
            else if (context.Result is EmptyResult)
            {
                context.Result = new ObjectResult(new M5Result(200, "操作成功"));
            }
        }

    }
}