﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using M5Extend.Tool;
using M5Standard.Core;
using M5Standard.Core.Aop;
using M5Standard.Entity;
using M5Standard.Entity.SqlSugar;
using M5Standard.Services.Dto;
using Microsoft.EntityFrameworkCore;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Services
{
    public interface ITestService2
    {
        Task<string> SayHello();
    }

    public class TestService2 : ApplicationService, ITestService2
    {
        [Duplication]
        [OperationLog("问好")]
        public async Task<string> SayHello()
        {
            return await Task.Run(() =>
             {
                 return "你好 M5";
             });
        }
    }


    public class TestService : ApplicationService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<User, long> _user;
        //private readonly ISqlSugarClient _susers;
        //private readonly ISimpleClient<User2> _suser22;
        public TestService(IRepository<User, long> user, IUnitOfWorkManager unitOfWorkManager
            //ISqlSugarClient susers
            )
        {
            _user = user;
            _unitOfWorkManager = unitOfWorkManager;
            //_susers = susers;
            //_suser22 = new SimpleClient<User2>(_susers);
        }

        //[Cached(300)]
        [Duplication, OperationLog("查询用户", true)]
        //[UnitOfWork(true)]
        public virtual async Task<List<UserDto>> GetAll2()
        {
            //var data = await _susers.Queryable<User2>().ToListAsync();
            //var data = await _suser22.GetListAsync();
            var insertUser = new User()
            {
                Age = 1111,
                Id = M5_Snowflake.CreateId(),
                IsDeleted = false,
                Name = "true111"
            };
            // _susers.Insertable(insertUser).ExecuteCommand();
            await _user.InsertAsync(insertUser);
            var data = await _user.GetAll().ToListAsync();
            return ObjectMapper.Map<List<UserDto>>(data); ;
            //return ObjectMapper.Map<List<UserDto>>(await _user.GetAll().ToListAsync());
        }

        public virtual async Task<User> GetById(long id)
        {
            return await _user.GetAll().FirstOrDefaultAsync(k => k.Id == id);
        }

        [UnitOfWork(false)]
        [OperationLog("新增用户", true)]
        public virtual async Task Insert(User user)
        {
            await _user.InsertAsync(user);
        }

        [UnitOfWork(false)]
        [OperationLog("删除用户")]
        public virtual async Task Delete(long id)
        {
            await _user.DeleteAsync(id);
        }
    }
}
