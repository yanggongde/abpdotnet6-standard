﻿using Abp.AutoMapper;
using M5Standard.Entity;
using M5Standard.Entity.SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Services.Dto
{
    [AutoMapFrom(typeof(User), typeof(User2))]
    public class UserDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
