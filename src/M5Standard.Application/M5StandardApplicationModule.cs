﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using M5Standard.Core;

namespace M5Standard
{
    [DependsOn(
        typeof(M5StandardCoreModule), 
        typeof(AbpAutoMapperModule))]
    [IgnoreScan]
    public class M5StandardApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(M5StandardApplicationModule).GetAssembly());
        }
    }
}