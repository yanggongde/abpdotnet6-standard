﻿using Abp.Application.Services;
using M5Standard.Core;

namespace M5Standard
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    [IgnoreScan]
    public abstract class M5StandardAppServiceBase : ApplicationService
    {
        protected M5StandardAppServiceBase()
        {
            LocalizationSourceName = M5StandardConsts.LocalizationSourceName;
        }
    }
}